import logo from './logo.svg';
import './App.css';
import ReactDOM from 'react-dom';
import { Component } from 'react';
import React from 'react';

const data = {
  name: "Cole Cheripko",
  imgURL:"https://towsontigers.com/images/2019/8/11/Cole_Cheripko.jpg?width=300",
  hobbyList: ["Playing Towson Football", "Lift Weights", "Hunting"]
}

function listHobbies(){
  return (
    <ol>
      {
        data.hobbyList.map(hob => (
          <li key={hob}>{hob}</li>
        ))
      }
    </ol>
  )
}


function App() {


  // const listItems = data.hobbyList.map((hob) =>
  //   <li key={hob}>{hob}</li>
  // );


  return (
    <div className="App">
      <p>{data.name}</p>
     <img src ={data.imgURL}></img>
     <p>Hobbies</p>
        <ul>
      {
        data.hobbyList.map(hob => (
          <li key={hob}>{hob}</li>
        ))
      }
    </ul>
    </div>
  );
  
}
export default App;